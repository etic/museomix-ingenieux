/**
 * Created by jean-christophecouture on 15-11-08.
 */

var url = "pinStatus.json"
var pinStatusGlobal;
var pinStatusGlobalOld;

function loadJSON(callback) {
    //http://codepen.io/KryptoniteDove/post/load-json-file-locally-using-pure-javascript

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}


function requestPinStatus() {
    loadJSON(function(response) {
        // Parse JSON string into object
        var actual_JSON = JSON.parse(response);
        //console.log(actual_JSON);
        //console.log(actual_JSON.status);
        pinStatusGlobal = actual_JSON.status;
    });
}
#!/usr/bin/env python

import cgi
import cgitb
from subprocess import call
import time
import RPi.GPIO as GPIO

cgitb.enable()

def main():

	# tell the GPIO module that we want 
	# to use the chip's pin numbering scheme
	GPIO.setmode(GPIO.BCM)
	# setup pin 25 as an output
	GPIO.setup(16,GPIO.OUT)
        GPIO.setup(20,GPIO.OUT)
        GPIO.setup(21,GPIO.OUT)

	while 1:
		# turn pin on 
		GPIO.output(16,True)
		time.sleep(0.5)
        	GPIO.output(20,True)
		time.sleep(0.5)
        	GPIO.output(21,True)
		time.sleep(2)
        	# turn pin off
        	GPIO.output(16,False)
		time.sleep(0.5)
        	GPIO.output(20,False)
		time.sleep(0.5)
        	GPIO.output(21,False)

	GPIO.cleanup()

if __name__=="__main__":
	main()

(function() {

  $("#switch-blue").bootstrapSwitch({
    // onText: "Blue",
    // offText: "Blue",
    labelText: "Blue",
    handleWidth: 40
  });

  $("#switch-red").bootstrapSwitch({
    // onText: "Red",
    // offText: "Red",
    labelText: "Red",
    handleWidth: 40
  });

  $("#switch-yellow").bootstrapSwitch({
    // onText: "Yellow",
    // offText: "Yellow",
    labelText: "Yellow",
    handleWidth: 40
  });

}).call(this);
